dotFiles
========
A repository for various portable shell and environment setup and app properties files
for Linux, OSx, and cygWin<br>
to include such files as:
.sh, .alias, .bashrc, .cmd, .bat, etc...

There will be a single install script for all platforms.
Said script will place the appropriate dot files in their proper locations, based on the particular $platform, $machine, and $user.
Thus, files will copied for CygWin and linked for linux.





