#!/bin/bash
myName=camsnaps
USAGE="$myName.sh [frameCount]"

# Parm must be a number
if [ "$1" -eq "$1" ] 2>/dev/null; then
	frameCount=$1
else
	frameCount=""
fi

~/bin/usbcamsnap.sh $frameCount
~/bin/picamsnap.sh $frameCount

