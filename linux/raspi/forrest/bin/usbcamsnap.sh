#!/bin/bash
myName=usbcamsnap
USAGE=$myName.sh [frameCount]
export usbimages=$images/usbcam

# Parm must be a number
if [ "$1" -eq "$1" ] 2>/dev/null; then
	frameCount=$1
else
	frameCount=""
fi

if [ ! -d $images ] 
then
    echo "Error: no dir $images"
    exit 1
else
    if [ ! -d $usbimages ]
    then
        mkdir -p $usbimages
    fi
    fswebcam -r 960x720 -d /dev/video0 $usbimages/image$frameCount.jpg
fi

