#!/bin/bash
myName=picamsnap
USAGE="$myName.sh [frameCount]"
export piimages=$images/picam

# Parm must be a number
if [ "$1" -eq "$1" ] 2>/dev/null; then
	frameCount=$1
else
	frameCount=""
fi

if [ ! -d $images ] 
then
    echo "Error: no dir $images"
    exit 1
else
    if [ ! -d $piimages ]
    then
        mkdir -p $piimages
    fi
    raspistill -t 125 -rot 270 -o $piimages/image$frameCount.jpg
fi


