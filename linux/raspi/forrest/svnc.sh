#!/bin/sh
# Smaller screen and less WIFI with 16 bit color: 1024x768 -depth 16
vncserver :0 -geometry 1024x768 -depth 16 -dpi 96
# More screen and best color: 1920x1080 -depth 24
# vncserver :0 -geometry 1920x1080 -depth 24 -dpi 96

